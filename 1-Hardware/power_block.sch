EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_opto
LIBS:IRS_Driver
LIBS:sensors
LIBS:arduino_shieldsNCL
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:module_ttl_rs485
LIBS:buffer_chain-cache
LIBS:driver_block-cache
LIBS:driver_block-rescue
LIBS:measurement_chain-cache
LIBS:measurement_chain_LV-cache
LIBS:microcontroller_block-cache
LIBS:non inverting buffer_chain-cache
LIBS:power_block-cache
LIBS:power_block-rescue
LIBS:quad_shottky_x2
LIBS:Power_converter_V4-cache
LIBS:Power_converter_V4.2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5450 2800 1    60   ~ 0
VgH
Text Label 3550 4450 2    60   ~ 0
VgL
Text Label 5100 2800 1    60   ~ 0
VsH
Text Label 3575 4850 2    60   ~ 0
VsL
Text Label 1450 3600 2    60   ~ 0
Vlow
Text Label 8550 3600 2    60   ~ 0
Vhigh
Text Label 7350 6150 3    60   ~ 0
ViH-
Text Label 7950 6150 3    60   ~ 0
ViH+
Text Label 1350 4700 2    60   ~ 0
VL+
Text Label 1500 5350 2    60   ~ 0
VL-
Text Label 8475 5150 0    60   ~ 0
VH-
$Comp
L IRF540N-RESCUE-Power_converter_V4 MH1
U 1 1 57304D53
P 5500 3500
F 0 "MH1" V 5850 3550 50  0000 L CNN
F 1 "FDPF39N20" V 5750 3250 50  0000 L CNN
F 2 "test:TO-220_Horizontal_inversé" H 5750 3425 50  0001 L CIN
F 3 "" H 5500 3500 50  0000 L CNN
	1    5500 3500
	0    1    1    0   
$EndComp
$Comp
L IRF540N-RESCUE-Power_converter_V4 ML1
U 1 1 573050C4
P 4225 4400
F 0 "ML1" H 4150 4125 50  0000 L CNN
F 1 "FDPF39N20" V 4500 4200 50  0000 L CNN
F 2 "test:TO-220_Horizontal_inversé" H 4475 4325 50  0001 L CIN
F 3 "" H 4225 4400 50  0000 L CNN
	1    4225 4400
	1    0    0    -1  
$EndComp
$Comp
L R RM1
U 1 1 592DA79D
P 5450 3075
F 0 "RM1" V 5530 3075 50  0000 C CNN
F 1 "2R2" V 5450 3075 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5380 3075 50  0001 C CNN
F 3 "" H 5450 3075 50  0000 C CNN
	1    5450 3075
	-1   0    0    1   
$EndComp
$Comp
L R RM2
U 1 1 592DAAA9
P 3800 4450
F 0 "RM2" V 3900 4450 50  0000 C CNN
F 1 "2R2" V 3800 4450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3730 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0000 C CNN
	1    3800 4450
	0    1    1    0   
$EndComp
$Comp
L L L1
U 1 1 592DC284
P 3375 3600
F 0 "L1" V 3325 3600 50  0000 C CNN
F 1 "MURATA 14xx" V 3475 3600 50  0000 C CNN
F 2 "7805:MURATA_Inductor_Big" H 3375 3600 50  0001 C CNN
F 3 "https://www.murata-ps.com/data/magnetics/kmp_1400.pdf" H 3375 3600 50  0001 C CNN
F 4 "??" V 3375 3600 60  0001 C CNN "Ref. RS"
	1    3375 3600
	0    -1   -1   0   
$EndComp
$Comp
L CP1 CPL2
U 1 1 592EA0B9
P 2300 4450
F 0 "CPL2" H 2325 4550 50  0000 L CNN
F 1 "C" H 2325 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 2300 4450 50  0001 C CNN
F 3 "" H 2300 4450 50  0000 C CNN
	1    2300 4450
	1    0    0    -1  
$EndComp
$Comp
L R RVL1
U 1 1 592EA443
P 1600 4125
F 0 "RVL1" V 1500 4125 50  0000 C CNN
F 1 "150K" V 1600 4125 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 1530 4125 50  0001 C CNN
F 3 "" H 1600 4125 50  0000 C CNN
	1    1600 4125
	1    0    0    -1  
$EndComp
$Comp
L R RVL2
U 1 1 592EA4CD
P 1600 4950
F 0 "RVL2" V 1500 4950 50  0000 C CNN
F 1 "10k" V 1600 4950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 1530 4950 50  0001 C CNN
F 3 "" H 1600 4950 50  0000 C CNN
	1    1600 4950
	1    0    0    -1  
$EndComp
$Comp
L R RVH3
U 1 1 592ED905
P 8125 4900
F 0 "RVH3" V 8025 4900 50  0000 C CNN
F 1 "10k" V 8125 4900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8055 4900 50  0001 C CNN
F 3 "" H 8125 4900 50  0000 C CNN
	1    8125 4900
	1    0    0    -1  
$EndComp
$Comp
L R RVH1
U 1 1 592ED98E
P 8125 3865
F 0 "RVH1" V 8025 3865 50  0000 C CNN
F 1 "150k" V 8125 3865 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8055 3865 50  0001 C CNN
F 3 "" H 8125 3865 50  0000 C CNN
	1    8125 3865
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02-RESCUE-Power_converter_V4 CNH1
U 1 1 59301D89
P 8950 4475
F 0 "CNH1" H 9150 4525 50  0000 C CNN
F 1 "High" H 9150 4425 50  0000 C CNN
F 2 "test:PhoenixContact_FFKDS-V1-5.08_02x7.62mm" H 8950 4475 50  0001 C CNN
F 3 "" H 8950 4475 50  0000 C CNN
	1    8950 4475
	1    0    0    -1  
$EndComp
$Comp
L LM35-LP S1
U 1 1 595B64EF
P 5700 6625
F 0 "S1" H 5450 6875 50  0000 C CNN
F 1 "LM35DZ" H 5750 6875 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5750 6375 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm35.pdf" H 5700 6625 50  0001 C CNN
	1    5700 6625
	1    0    0    -1  
$EndComp
Text Notes 900  600  0    60   ~ 0
BLOCK INPUTS
Text Notes 6950 650  0    60   ~ 0
BLOCK OUTPUTS
Text HLabel 1050 850  0    60   Input ~ 0
VgH
Text HLabel 1050 1050 0    60   Input ~ 0
VsH
Text HLabel 1050 1250 0    60   Input ~ 0
VgL
Text HLabel 1050 1450 0    60   Input ~ 0
VsL
Text Label 1850 850  0    60   ~ 0
VgH
Text Label 1850 1250 0    60   ~ 0
VgL
Text Label 1850 1050 0    60   ~ 0
VsH
Text Label 1850 1450 0    60   ~ 0
VsL
Text Notes 700  2000 0    60   ~ 0
CONVERTER INPUT
Text Notes 650  2150 0    60   ~ 0
----------------------------
Text Label 2325 6150 0    60   ~ 0
ViL-
Text HLabel 7550 1350 2    60   Output ~ 0
ViH+
Text HLabel 7550 1550 2    60   Output ~ 0
ViH-
Text Label 8425 4625 0    60   ~ 0
VH+
Text Notes 650  2450 0    60   ~ 0
The inputs of this block are the gate-source \nvoltages for the high-side (H) and low-side (L) transistors. \n(VgH-VsH/VgL-VsL)\n
Text Notes 8950 4275 0    60   ~ 0
There are 5 measurements spread\namong the power elements:\n\nViL+ and ViL- is the voltage drop \nof the shunt resistance reading low side current\nEQUIVALECE: 1 Amp = 13mV (3 shunts)\n\nViH+ and ViH- is the voltage drop \nof the shunt resistance reading high side current\nEQUIVALECE: 1 Amp = 13mV  (3 shunts)\n\nVL+ and VL- is the voltage drop \nof the resistance reading low side voltage\nEQUIVALECE: 1/15 = 60V = 4V \nIMPLEMENTED: 1/16 = 60V = 3.75V \n\nVH+ and VH- is the voltage drop \nof the resistance reading high side voltage\nEQUIVALECE: 1/30 = 120V = 4V \nIMPLEMENTED: 1/31 = 120V = 3.87V\n \nTemperature\n10mV per  °C as per the component's datasheet\n \n
Text Notes 8975 2025 0    60   ~ 0
MEASUREMENTS
Text Notes 8975 2125 0    60   ~ 0
-------------------------
Text Notes 2800 1200 0    60   ~ 0
This topology employs capacitors\nboth at the input and the output. \nThe inductance in the middle allows it\nto be a fully reversible buck-boost.
Text Notes 2900 650  0    60   ~ 0
DETAILS OF THE POWER BLOCK
Text Notes 2900 750  0    60   ~ 0
-----------------------------------------------
Text Notes 4650 1500 0    60   ~ 0
This version of the converter uses 200V\nMOSFETS capable of handling up to 20A. \nThis converter was designed to have a \nhigh-side up to 120V making its low-side \nreasonable up to 60V.\nThe power in this structure HAS  \nNOT BEEN TESTED beyond 500W. 
Text HLabel 8650 1150 2    60   Output ~ 0
VL-
Text HLabel 8650 1350 2    60   Output ~ 0
VH+
Text HLabel 8650 1550 2    60   Output ~ 0
VH-
Text Label 6900 1550 0    60   ~ 0
ViH-
Text Label 6900 1350 0    60   ~ 0
ViH+
Text Label 8050 1350 0    60   ~ 0
VH+
Text Label 8050 1550 0    60   ~ 0
VH-
Text Label 8050 1150 0    60   ~ 0
VL-
Text HLabel 10600 900  2    60   Output ~ 0
Vlow
Text HLabel 10600 1100 2    60   Output ~ 0
Vhigh
Text Label 10250 1100 0    60   ~ 0
Vhigh
Text Label 10250 900  0    60   ~ 0
Vlow
$Comp
L CONN_01X02-RESCUE-Power_converter_V4 CNL1
U 1 1 595D128A
P 900 4475
F 0 "CNL1" H 1100 4525 50  0000 C CNN
F 1 "Low" H 1100 4425 50  0000 C CNN
F 2 "test:PhoenixContact_FFKDS-V1-5.08_02x7.62mm" H 900 4475 50  0001 C CNN
F 3 "" H 900 4475 50  0000 C CNN
	1    900  4475
	-1   0    0    -1  
$EndComp
Text Label 4525 6175 0    60   ~ 0
+5VHigh
Text Notes 5650 5800 0    60   ~ 0
TEMPERATURE SENSOR
Text Label 6450 6625 0    60   ~ 0
VT1+
Text Label 6450 7350 0    60   ~ 0
VT1-
Text Label 9100 950  0    60   ~ 0
VT1+
Text Label 9100 1150 0    60   ~ 0
VT1-
Text HLabel 9650 950  2    60   Output ~ 0
VT1+
Text HLabel 9650 1150 2    60   Output ~ 0
VT1-
Text HLabel 1050 1600 0    60   Input ~ 0
+5VHigh
Text Label 1850 1600 0    60   ~ 0
+5VHigh
$Comp
L C Cdt1
U 1 1 596E2E43
P 5100 6925
F 0 "Cdt1" H 5125 7025 50  0000 L CNN
F 1 "100n" H 5125 6825 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5138 6775 50  0001 C CNN
F 3 "" H 5100 6925 50  0000 C CNN
	1    5100 6925
	1    0    0    -1  
$EndComp
Text HLabel 10550 1500 2    60   Output ~ 0
GNDPwR
Text Label 6900 950  0    60   ~ 0
ViL+
Text Label 6900 1150 0    60   ~ 0
ViL-
Text HLabel 7550 1150 2    60   Output ~ 0
ViL-
Text HLabel 7550 950  2    60   Output ~ 0
ViL+
Text Label 1725 6150 0    60   ~ 0
ViL+
$Comp
L CP1 CPL1
U 1 1 599FEEC6
P 1950 4450
F 0 "CPL1" H 1975 4550 50  0000 L CNN
F 1 "C" H 1975 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 1950 4450 50  0001 C CNN
F 3 "" H 1950 4450 50  0000 C CNN
	1    1950 4450
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPL3
U 1 1 599FF056
P 2650 4450
F 0 "CPL3" H 2675 4550 50  0000 L CNN
F 1 "C" H 2675 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 2650 4450 50  0001 C CNN
F 3 "" H 2650 4450 50  0000 C CNN
	1    2650 4450
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPL4
U 1 1 599FF0E4
P 3000 4450
F 0 "CPL4" H 3025 4550 50  0000 L CNN
F 1 "C" H 3025 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 3000 4450 50  0001 C CNN
F 3 "" H 3000 4450 50  0000 C CNN
	1    3000 4450
	1    0    0    -1  
$EndComp
Text Label 10200 1500 2    60   ~ 0
GNDPwR
Text Label 2875 5525 2    60   ~ 0
GNDPwR
Text Label 5975 7525 2    60   ~ 0
GNDPwR
Text Label 8050 975  0    60   ~ 0
VL+
Text HLabel 8650 975  2    60   Output ~ 0
VL+
$Comp
L R RiH1
U 1 1 5ABE4E12
P 7650 5350
F 0 "RiH1" V 7730 5350 50  0000 C CNN
F 1 "0.04" V 7650 5350 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 7580 5350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 7650 5350 50  0001 C CNN
	1    7650 5350
	0    1    1    0   
$EndComp
$Comp
L R RiH2
U 1 1 5ABE4E1B
P 7650 5650
F 0 "RiH2" V 7730 5650 50  0000 C CNN
F 1 "0.04" V 7650 5650 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 7580 5650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 7650 5650 50  0001 C CNN
	1    7650 5650
	0    1    1    0   
$EndComp
$Comp
L R RiH3
U 1 1 5ABE4E21
P 7650 5900
F 0 "RiH3" V 7730 5900 50  0000 C CNN
F 1 "0.04" V 7650 5900 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 7580 5900 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 7650 5900 50  0001 C CNN
	1    7650 5900
	0    1    1    0   
$EndComp
$Comp
L R RiL1
U 1 1 5ABE679F
P 2025 5350
F 0 "RiL1" V 2105 5350 50  0000 C CNN
F 1 "0.04" V 2025 5350 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 1955 5350 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 2025 5350 50  0001 C CNN
F 4 "487-9131" V 2025 5350 60  0001 C CNN "Ref. RS"
	1    2025 5350
	0    1    1    0   
$EndComp
$Comp
L R RiL2
U 1 1 5ABE67A5
P 2025 5650
F 0 "RiL2" V 2105 5650 50  0000 C CNN
F 1 "0.04" V 2025 5650 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 1955 5650 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 2025 5650 50  0001 C CNN
	1    2025 5650
	0    1    1    0   
$EndComp
$Comp
L R RiL3
U 1 1 5ABE67AB
P 2025 5900
F 0 "RiL3" V 2105 5900 50  0000 C CNN
F 1 "0.04" V 2025 5900 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 1955 5900 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/0496/0900766b80496a3c.pdf" H 2025 5900 50  0001 C CNN
	1    2025 5900
	0    1    1    0   
$EndComp
$Comp
L C CVL1
U 1 1 5ABF0E7A
P 1810 4960
F 0 "CVL1" H 1835 5060 50  0000 L CNN
F 1 "100n" H 1835 4860 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1848 4810 50  0001 C CNN
F 3 "" H 1810 4960 50  0000 C CNN
	1    1810 4960
	1    0    0    -1  
$EndComp
Wire Notes Line
	2450 1750 2450 650 
Wire Notes Line
	6750 1750 6750 650 
Wire Wire Line
	3950 4450 4025 4450
Wire Wire Line
	5450 3300 5450 3225
Wire Wire Line
	3525 3600 5300 3600
Wire Wire Line
	4325 3600 4325 4200
Wire Wire Line
	1850 850  1050 850 
Wire Wire Line
	1850 1050 1050 1050
Wire Wire Line
	1850 1250 1050 1250
Wire Wire Line
	1850 1450 1050 1450
Wire Notes Line
	8850 1900 11050 1900
Wire Wire Line
	7550 950  6900 950 
Wire Wire Line
	7550 1150 6900 1150
Wire Wire Line
	7550 1350 6900 1350
Wire Wire Line
	7550 1550 6900 1550
Wire Wire Line
	8650 1150 8050 1150
Wire Wire Line
	8650 1350 8050 1350
Wire Wire Line
	8650 1550 8050 1550
Wire Wire Line
	10600 900  10250 900 
Wire Wire Line
	10600 1100 10250 1100
Wire Wire Line
	4325 5350 4325 4600
Wire Wire Line
	3650 4450 3550 4450
Wire Wire Line
	5450 2925 5450 2800
Wire Wire Line
	1100 3600 3225 3600
Wire Wire Line
	3575 4850 4725 4850
Connection ~ 4325 4850
Wire Wire Line
	5100 2800 5100 4075
Connection ~ 5100 3600
Wire Wire Line
	1600 4275 1600 4800
Wire Wire Line
	1600 4700 1350 4700
Connection ~ 1600 4700
Wire Wire Line
	5700 3600 8750 3600
Wire Wire Line
	8125 4015 8125 4185
Wire Wire Line
	8125 4485 8125 4750
Wire Wire Line
	6775 3600 6775 4350
Connection ~ 6775 3600
Wire Wire Line
	8125 4625 8425 4625
Connection ~ 8125 4625
Wire Wire Line
	8125 5350 8125 5050
Wire Wire Line
	6775 5350 6775 4650
Wire Wire Line
	8125 5150 8475 5150
Connection ~ 8125 5150
Wire Wire Line
	5850 4075 5850 3600
Connection ~ 5850 3600
Wire Notes Line
	8825 2050 8825 4100
Wire Notes Line
	11025 4250 8875 4250
Wire Wire Line
	5100 7350 6450 7350
Wire Wire Line
	5700 7350 5700 6925
Wire Wire Line
	5700 6175 5700 6325
Wire Wire Line
	4525 6175 5700 6175
Wire Wire Line
	6050 7350 6050 7525
Connection ~ 6050 7350
Wire Wire Line
	9100 950  9650 950 
Wire Wire Line
	9100 1150 9650 1150
Wire Notes Line
	2450 650  550  650 
Wire Notes Line
	500  1700 2450 1700
Wire Notes Line
	6750 1750 11150 1750
Wire Notes Line
	6850 750  11050 750 
Wire Wire Line
	1850 1600 1050 1600
Wire Wire Line
	10200 1500 10550 1500
Wire Wire Line
	1600 3600 1600 3975
Connection ~ 1600 3600
Wire Wire Line
	1950 4200 3000 4200
Wire Wire Line
	3000 4200 3000 4300
Wire Wire Line
	1950 4200 1950 4300
Wire Wire Line
	1950 4600 1950 4700
Wire Wire Line
	1950 4700 3000 4700
Wire Wire Line
	3000 4700 3000 4600
Wire Wire Line
	2300 4200 2300 4300
Connection ~ 2300 4200
Wire Wire Line
	2300 4600 2300 4700
Connection ~ 2300 4700
Wire Wire Line
	2650 4300 2650 4200
Connection ~ 2650 4200
Wire Wire Line
	2650 4600 2650 4700
Connection ~ 2650 4700
Wire Wire Line
	2450 3600 2450 4200
Connection ~ 2450 4200
Connection ~ 2450 3600
Wire Wire Line
	2450 5350 2450 4700
Connection ~ 2450 4700
Wire Wire Line
	7125 4350 7125 3600
Connection ~ 7125 3600
Wire Wire Line
	7125 5350 7125 4650
Wire Wire Line
	6125 4350 6125 3600
Connection ~ 6125 3600
Wire Wire Line
	6475 3600 6475 4350
Connection ~ 6475 3600
Wire Wire Line
	6475 5350 6475 4650
Wire Wire Line
	6125 5350 6125 4650
Wire Wire Line
	5100 6775 5100 6175
Connection ~ 5100 6175
Wire Wire Line
	5100 7075 5100 7350
Connection ~ 5700 7350
Wire Wire Line
	1600 5100 1600 5350
Wire Wire Line
	3025 5525 2875 5525
Wire Wire Line
	3025 5350 3025 5525
Wire Wire Line
	6050 7525 5975 7525
Wire Wire Line
	4725 4100 4325 4100
Connection ~ 4325 4100
Connection ~ 4325 3600
Wire Wire Line
	6450 6625 6100 6625
Wire Wire Line
	8650 975  8050 975 
Wire Wire Line
	7950 5350 7950 6150
Wire Wire Line
	7500 5650 7350 5650
Wire Wire Line
	7800 5650 7950 5650
Connection ~ 7950 5650
Wire Wire Line
	7350 5900 7500 5900
Wire Wire Line
	7800 5900 7950 5900
Connection ~ 7950 5900
Wire Wire Line
	8125 3600 8125 3715
Connection ~ 8125 3600
Wire Wire Line
	7350 5350 7350 6150
Connection ~ 7350 5900
Connection ~ 7350 5650
Wire Wire Line
	7800 5350 8750 5350
Connection ~ 7950 5350
Wire Wire Line
	2325 5350 2325 6150
Wire Wire Line
	1875 5650 1725 5650
Wire Wire Line
	2175 5650 2325 5650
Connection ~ 2325 5650
Wire Wire Line
	1725 5900 1875 5900
Wire Wire Line
	2175 5900 2325 5900
Connection ~ 2325 5900
Connection ~ 1725 5900
Connection ~ 1725 5650
Wire Wire Line
	1725 5350 1725 6150
Wire Wire Line
	1100 5350 1875 5350
Connection ~ 1600 5350
Connection ~ 1725 5350
Wire Wire Line
	2175 5350 7500 5350
Connection ~ 2325 5350
Connection ~ 2450 5350
Connection ~ 3025 5350
Connection ~ 4325 5350
Connection ~ 6125 5350
Connection ~ 6475 5350
Connection ~ 6775 5350
Connection ~ 7125 5350
Connection ~ 7350 5350
Wire Wire Line
	1100 4425 1100 3600
Wire Wire Line
	1100 4525 1100 5350
Wire Notes Line
	6850 5850 4325 5850
Wire Notes Line
	4325 5850 4325 7650
Wire Wire Line
	8750 3600 8750 4425
Wire Wire Line
	8750 5350 8750 4525
Connection ~ 8125 5350
Wire Wire Line
	1810 4810 1810 4635
Wire Wire Line
	1810 4635 1600 4635
Connection ~ 1600 4635
Wire Wire Line
	1810 5110 1810 5350
Connection ~ 1810 5350
$Comp
L C CVH1
U 1 1 5ABF19EC
P 7865 4925
F 0 "CVH1" H 7890 5025 50  0000 L CNN
F 1 "100n" H 7890 4825 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 7903 4775 50  0001 C CNN
F 3 "" H 7865 4925 50  0000 C CNN
	1    7865 4925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7865 4775 7865 4690
Wire Wire Line
	7865 4690 8125 4690
Connection ~ 8125 4690
Wire Wire Line
	7865 5075 7865 5350
Connection ~ 7865 5350
$Comp
L R RVH2
U 1 1 5AC58938
P 8125 4335
F 0 "RVH2" V 8025 4335 50  0000 C CNN
F 1 "150k" V 8125 4335 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8055 4335 50  0001 C CNN
F 3 "" H 8125 4335 50  0000 C CNN
	1    8125 4335
	1    0    0    -1  
$EndComp
$Comp
L MBR20200 DL1
U 1 1 5AC658D9
P 4725 4400
F 0 "DL1" H 4600 4800 50  0000 C CNN
F 1 "MBR20200" V 4500 4475 50  0000 C CNN
F 2 "test:TO-220_Horizontal_inversé" H 4725 4400 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBR20200CT-D.PDF" H 4725 4400 50  0001 C CNN
	1    4725 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4625 4750 4825 4750
Wire Wire Line
	4725 4850 4725 4750
Connection ~ 4725 4750
Wire Wire Line
	4725 4200 4725 4100
$Comp
L MBR20200 DH1
U 1 1 5AC66389
P 5550 4075
F 0 "DH1" V 5450 3950 50  0000 C CNN
F 1 "MBR20200" V 5325 4150 50  0000 C CNN
F 2 "test:TO-220_Horizontal_inversé" H 5550 4075 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBR20200CT-D.PDF" H 5550 4075 50  0001 C CNN
	1    5550 4075
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 3975 5200 4175
Wire Wire Line
	5100 4075 5200 4075
Connection ~ 5200 4075
Wire Wire Line
	5750 4075 5850 4075
$Comp
L CP1 CPH1
U 1 1 59B6573D
P 6125 4500
F 0 "CPH1" H 6150 4600 50  0000 L CNN
F 1 "C" H 6150 4400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6125 4500 50  0001 C CNN
F 3 "" H 6125 4500 50  0000 C CNN
	1    6125 4500
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPH2
U 1 1 59B65685
P 6475 4500
F 0 "CPH2" H 6500 4600 50  0000 L CNN
F 1 "C" H 6500 4400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6475 4500 50  0001 C CNN
F 3 "" H 6475 4500 50  0000 C CNN
	1    6475 4500
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPH4
U 1 1 59A6CF5C
P 7125 4500
F 0 "CPH4" H 7150 4600 50  0000 L CNN
F 1 "C" H 7150 4400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 7125 4500 50  0001 C CNN
F 3 "" H 7125 4500 50  0000 C CNN
	1    7125 4500
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPH3
U 1 1 592ED805
P 6775 4500
F 0 "CPH3" H 6800 4600 50  0000 L CNN
F 1 "C" H 6800 4400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6775 4500 50  0001 C CNN
F 3 "" H 6775 4500 50  0000 C CNN
	1    6775 4500
	1    0    0    -1  
$EndComp
Text Notes 7375 7500 0    60   ~ 0
POWER BLOCK
$EndSCHEMATC
