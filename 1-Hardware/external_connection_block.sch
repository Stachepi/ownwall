EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_opto
LIBS:IRS_Driver
LIBS:sensors
LIBS:arduino_shieldsNCL
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:module_ttl_rs485
LIBS:buffer_chain-cache
LIBS:driver_block-cache
LIBS:driver_block-rescue
LIBS:measurement_chain-cache
LIBS:measurement_chain_LV-cache
LIBS:microcontroller_block-cache
LIBS:non inverting buffer_chain-cache
LIBS:power_block-cache
LIBS:power_block-rescue
LIBS:quad_shottky_x2
LIBS:Power_converter_V4-cache
LIBS:Power_converter_V4.2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 750  650  0    60   ~ 0
Input Variables
Text Label 1450 1300 2    60   ~ 0
GNDLow
Text Label 1450 1100 2    60   ~ 0
+5VLow
Text HLabel 1050 1300 0    60   Input ~ 0
GNDLow
Text HLabel 1050 1100 0    60   Input ~ 0
+5VLow
Text Label 2600 1250 2    60   ~ 0
DE/RE
Text Label 2600 1050 2    60   ~ 0
DI
Text HLabel 2200 1250 0    60   Input ~ 0
DE/RE
Text HLabel 2200 1050 0    60   Input ~ 0
DI
Text Notes 11000 700  2    60   ~ 0
Output Variables
Text Notes 5300 750  0    79   ~ 0
COMMUNICATION BLOCK
Text Notes 5150 1300 0    59   ~ 0
DI - Driver inpout (to Tx pin of arduino)\nDE/RE -Driver Output Enable (high to enable)/Receiver output Enable (Low to enable)\nRO - Receiver Output (to Rx pin or Arduino)
$Comp
L Module_TTL_RS485-RESCUE-Power_converter_V4 MOD1
U 1 1 59777A6E
P 7100 3650
F 0 "MOD1" H 7900 3975 60  0000 C CNN
F 1 "Module_TTL_RS485" H 8000 3575 60  0000 C CNN
F 2 "shield_arduino_footprint:ModuleTTLRS485" H 7125 3550 60  0001 C CNN
F 3 "" H 7125 3550 60  0000 C CNN
	1    7100 3650
	-1   0    0    1   
$EndComp
NoConn ~ 7500 3850
NoConn ~ 7500 3950
Text Label 7500 3350 2    60   ~ 0
+5VLow
Text Label 7500 4550 2    60   ~ 0
GNDLow
Wire Wire Line
	1050 1300 1450 1300
Wire Wire Line
	1050 1100 1450 1100
Wire Wire Line
	2200 1250 2600 1250
Wire Wire Line
	2200 1050 2600 1050
Wire Notes Line
	10000 550  10000 2500
Wire Notes Line
	10000 2500 11100 2500
Wire Notes Line
	550  1900 4650 1900
Wire Notes Line
	4650 1900 4650 600 
Wire Notes Line
	5100 850  7400 850 
Wire Notes Line
	650  700  1450 700 
Wire Notes Line
	10250 750  11050 750 
Wire Wire Line
	7300 4050 7500 4050
Wire Wire Line
	5200 3950 4900 3950
Wire Wire Line
	5200 4050 4900 4050
Wire Wire Line
	4550 3850 5200 3850
Wire Wire Line
	5200 3750 4900 3750
Wire Wire Line
	7300 3950 7500 3950
Wire Wire Line
	7300 3850 7500 3850
Wire Wire Line
	7300 3750 7500 3750
Wire Wire Line
	7500 3750 7500 3350
Wire Wire Line
	7500 4050 7500 4550
Wire Wire Line
	10650 1000 10250 1000
Text HLabel 10650 1000 2    60   Output ~ 0
RO
Text Label 10250 1000 0    60   ~ 0
RO
Text Label 4900 4050 0    60   ~ 0
DI
Text Label 4900 3750 0    60   ~ 0
RO
Wire Wire Line
	4900 3950 4900 3850
Connection ~ 4900 3850
Text Label 4550 3850 0    60   ~ 0
DE/RE
Text Notes 7375 7500 0    79   ~ 0
EXTERNAL COMMUNICATION BLOCK
$EndSCHEMATC
