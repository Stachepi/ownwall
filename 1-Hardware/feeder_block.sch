EESchema Schematic File Version 2
LIBS:Power_converter_V4-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_opto
LIBS:IRS_Driver
LIBS:sensors
LIBS:arduino_shieldsNCL
LIBS:Measurement_block-cache
LIBS:Measurement_block-rescue
LIBS:digital
LIBS:lm2672
LIBS:digital-cache
LIBS:feeder_block-cache
LIBS:module_ttl_rs485
LIBS:buffer_chain-cache
LIBS:driver_block-cache
LIBS:driver_block-rescue
LIBS:measurement_chain-cache
LIBS:measurement_chain_LV-cache
LIBS:microcontroller_block-cache
LIBS:non inverting buffer_chain-cache
LIBS:power_block-cache
LIBS:power_block-rescue
LIBS:quad_shottky_x2
LIBS:Power_converter_V4-cache
LIBS:Power_converter_V4.2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 850  650  0    60   ~ 0
BLOCK INPUTS
Text Notes 850  800  0    60   ~ 0
----------------------------
Text Notes 8250 700  0    60   ~ 0
BLOCK OUTPUTS
Text Notes 8250 850  0    60   ~ 0
----------------------------
Text Notes 3650 1350 0    60   ~ 0
This is the global feeder circuit. \nIn this solution, the feeder is composed of a DC-DC\nconverter that regulates 15V out of the Vlow. \nThese 15V are used to create 5V for the high side\nand 5V for the low side.\nThe masses are separated.
Text Notes 3650 700  0    60   ~ 0
FEEDER DETAILS
Text Notes 3650 800  0    60   ~ 0
-----------------------------
Text HLabel 1050 950  0    60   Input ~ 0
Vlow
Text HLabel 1050 1150 0    60   Input ~ 0
Vhigh
Text Label 1650 1150 2    60   ~ 0
Vhigh
Text Label 1650 950  2    60   ~ 0
Vlow
Text HLabel 1050 1400 0    60   Input ~ 0
GNDPwR
Text Label 1650 1400 2    60   ~ 0
GNDPwR
Text HLabel 8950 1000 2    60   Output ~ 0
+5VLow
Text Label 8300 1000 0    60   ~ 0
+5VLow
Text HLabel 8950 1150 2    60   Output ~ 0
GNDLow
Text Label 8300 1150 0    60   ~ 0
GNDLow
Text Notes 5000 7700 1    60   ~ 0
WARNING: MASSES MUST BE SEPARATED!!!
Text HLabel 10400 1150 2    60   Output ~ 0
+5VHigh
Text Label 9750 1150 0    60   ~ 0
+5VHigh
Text HLabel 10400 1000 2    60   Output ~ 0
+15VHigh
Text Label 9750 1000 0    60   ~ 0
+15VHigh
Text HLabel 10400 1300 2    60   Output ~ 0
GNDPwR
Text Label 9750 1300 0    60   ~ 0
GNDPwR
$Comp
L CP CH6
U 1 1 59765087
P 6100 4500
F 0 "CH6" H 6125 4600 50  0000 L CNN
F 1 "20u" H 6125 4400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 6138 4350 50  0001 C CNN
F 3 "" H 6100 4500 50  0000 C CNN
	1    6100 4500
	1    0    0    -1  
$EndComp
$Comp
L R RH2
U 1 1 597664C7
P 5500 4750
F 0 "RH2" V 5580 4750 50  0000 C CNN
F 1 "10k" V 5500 4750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5430 4750 50  0001 C CNN
F 3 "" H 5500 4750 50  0000 C CNN
	1    5500 4750
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-Power_converter_V4 LDH2
U 1 1 597664CD
P 5500 4300
F 0 "LDH2" H 5500 4400 50  0000 C CNN
F 1 "LED" H 5500 4200 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5500 4300 50  0001 C CNN
F 3 "" H 5500 4300 50  0000 C CNN
	1    5500 4300
	0    -1   -1   0   
$EndComp
$Comp
L LMR16010 Uf1
U 1 1 5976D566
P 3000 4050
F 0 "Uf1" H 2555 3785 60  0000 C CNN
F 1 "LMR16010" V 3575 4450 60  0000 C CNN
F 2 "test:SOIC-8-1EP_3.9x4.9mm_Pitch1.27mm_thermal++" H 2885 3980 60  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmr16010.pdf" H 2885 3980 60  0001 C CNN
	1    3000 4050
	0    1    1    0   
$EndComp
$Comp
L C Cb1
U 1 1 5976EAE8
P 3900 3750
F 0 "Cb1" H 3925 3850 50  0000 L CNN
F 1 "100n" H 3925 3650 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3938 3600 50  0001 C CNN
F 3 "" H 3900 3750 50  0000 C CNN
	1    3900 3750
	0    -1   -1   0   
$EndComp
$Comp
L L Ls1
U 1 1 5976F377
P 4400 4050
F 0 "Ls1" V 4350 4050 50  0000 C CNN
F 1 "33u" V 4475 4050 50  0000 C CNN
F 2 "digital:inductance_murata_1200LRS" H 4400 4050 50  0001 C CNN
F 3 "" H 4400 4050 50  0000 C CNN
F 4 "Murata family 1200LRS" V 4400 4050 60  0001 C CNN "Notes"
	1    4400 4050
	0    -1   -1   0   
$EndComp
$Comp
L D_Schottky Ds1
U 1 1 5976F8F9
P 3900 4300
F 0 "Ds1" H 3900 4400 50  0000 C CNN
F 1 "MBR2H100SFT3G" H 3900 4200 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 3900 4300 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1221/0900766b812212a8.pdf" H 3900 4300 50  0001 C CNN
F 4 "781-5650" H 3900 4300 60  0001 C CNN "Ref. RS"
	1    3900 4300
	0    1    1    0   
$EndComp
$Comp
L R Rt2
U 1 1 597713E8
P 2150 4675
F 0 "Rt2" V 2230 4675 50  0000 C CNN
F 1 "10k" V 2150 4675 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2080 4675 50  0001 C CNN
F 3 "" H 2150 4675 50  0000 C CNN
	1    2150 4675
	1    0    0    -1  
$EndComp
$Comp
L R Rfbt1
U 1 1 59771C86
P 4800 4250
F 0 "Rfbt1" V 4880 4250 50  0000 C CNN
F 1 "150k" V 4800 4250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4730 4250 50  0001 C CNN
F 3 "" H 4800 4250 50  0000 C CNN
	1    4800 4250
	1    0    0    -1  
$EndComp
Text Label 1200 3350 2    60   ~ 0
Vlow
Text Label 4300 4600 2    60   ~ 0
GNDPwR
Text Label 5350 3600 2    60   ~ 0
+15VLMR
$Comp
L TME/CRE Uf3
U 1 1 59777E0C
P 4950 6800
F 0 "Uf3" H 4500 7250 60  0000 C CNN
F 1 "TME/CRE" H 5350 7250 60  0000 C CNN
F 2 "test:TME_0505s" H 4835 6730 60  0001 C CNN
F 3 "http://www.tme.eu/en/Document/ecc45a2bcbf6e1314ca25a1f4efc1d56/TME-SERIES-EN.pdf" H 4835 6730 60  0001 C CNN
	1    4950 6800
	1    0    0    -1  
$EndComp
Text Notes 595  5800 0    60   ~ 0
DC low drop out linear regulator \n5V supply on the high side
Text Label 710  6680 0    60   ~ 0
+15VHigh
Text Label 1770 7315 2    60   ~ 0
GNDPwR
Text Label 3235 6465 2    60   ~ 0
+5VHigh
Text Label 4000 7600 0    60   ~ 0
+5VHigh
Text Label 3975 7475 0    60   ~ 0
GNDPwR
Text Label 5800 7600 2    60   ~ 0
GNDLow
$Comp
L R RL1
U 1 1 572042AC
P 5950 7100
F 0 "RL1" V 6030 7100 50  0000 C CNN
F 1 "10k" V 5950 7100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5880 7100 50  0001 C CNN
F 3 "" H 5950 7100 50  0000 C CNN
	1    5950 7100
	1    0    0    -1  
$EndComp
Text Notes 6200 5750 0    60   ~ 0
LOW VOLTAGE SIDE FEEDER
Text Notes 5550 5850 0    60   ~ 0
----------------------------
Text Notes 5900 6250 0    60   ~ 0
This component generates \n+5V for the low voltage side reference. \nThe footprint is compatible with both\nMurata and Traco components.\n
$Comp
L LED-RESCUE-Power_converter_V4 LDL1
U 1 1 595BC656
P 5950 6700
F 0 "LDL1" H 5950 6800 50  0000 C CNN
F 1 "LED" H 5950 6600 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5950 6700 50  0001 C CNN
F 3 "" H 5950 6700 50  0000 C CNN
	1    5950 6700
	0    -1   -1   0   
$EndComp
Text Label 6350 6500 2    60   ~ 0
+5VLow
$Comp
L POT-RESCUE-Power_converter_V4 PH1
U 1 1 59787B66
P 4800 5050
F 0 "PH1" V 4700 5050 50  0000 C CNN
F 1 "10k" V 4800 5050 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 4800 5050 50  0001 C CNN
F 3 "" H 4800 5050 50  0000 C CNN
	1    4800 5050
	1    0    0    -1  
$EndComp
$Comp
L CP CH1
U 1 1 5978D844
P 1350 4050
F 0 "CH1" H 1375 4150 50  0000 L CNN
F 1 "20u" H 1375 3950 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1388 3900 50  0001 C CNN
F 3 "" H 1350 4050 50  0000 C CNN
	1    1350 4050
	1    0    0    -1  
$EndComp
$Comp
L C CH2
U 1 1 5978E38C
P 1750 4050
F 0 "CH2" H 1775 4150 50  0000 L CNN
F 1 "4u7" H 1775 3950 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1788 3900 50  0001 C CNN
F 3 "" H 1750 4050 50  0000 C CNN
	1    1750 4050
	1    0    0    -1  
$EndComp
Text HLabel 8950 1300 2    60   Output ~ 0
Pgood
Text Label 8300 1300 0    60   ~ 0
Pgood
Text Label 2600 5250 0    60   ~ 0
Pgood
$Comp
L LED-RESCUE-Power_converter_V4 LDH1
U 1 1 599EACA5
P 2955 6715
F 0 "LDH1" H 2955 6815 50  0000 C CNN
F 1 "LED" H 2955 6615 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 2955 6715 50  0001 C CNN
F 3 "" H 2955 6715 50  0000 C CNN
	1    2955 6715
	0    -1   -1   0   
$EndComp
$Comp
L R Rld2
U 1 1 599EB330
P 2955 7115
F 0 "Rld2" V 3055 7115 50  0000 C CNN
F 1 "10k" V 2955 7115 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2885 7115 50  0001 C CNN
F 3 "" H 2955 7115 50  0000 C CNN
	1    2955 7115
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 Tf1
U 1 1 599F0400
P 6660 1100
F 0 "Tf1" H 6660 1200 50  0000 C CNN
F 1 "CON1X1" V 6760 1150 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6660 1100 50  0001 C CNN
F 3 "" H 6660 1100 50  0000 C CNN
	1    6660 1100
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 Tf2
U 1 1 599F047A
P 6660 1350
F 0 "Tf2" H 6660 1450 50  0000 C CNN
F 1 "CON1X1" V 6760 1350 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6660 1350 50  0001 C CNN
F 3 "" H 6660 1350 50  0000 C CNN
	1    6660 1350
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 Tf3
U 1 1 599F0516
P 6660 1600
F 0 "Tf3" H 6660 1700 50  0000 C CNN
F 1 "CON1X1" V 6760 1600 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6660 1600 50  0001 C CNN
F 3 "" H 6660 1600 50  0000 C CNN
	1    6660 1600
	-1   0    0    1   
$EndComp
Text Label 7110 1100 0    60   ~ 0
+15VHigh
Text Label 7110 1350 0    60   ~ 0
+5VHigh
Text Label 7110 1600 0    60   ~ 0
+5VLow
Text Label 9050 3800 2    60   ~ 0
Vhigh
$Comp
L C_Small Cld1
U 1 1 59E04A8C
P 1470 6715
F 0 "Cld1" H 1480 6785 50  0000 L CNN
F 1 "4u7" H 1480 6635 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1470 6715 50  0001 C CNN
F 3 "" H 1470 6715 50  0001 C CNN
	1    1470 6715
	1    0    0    -1  
$EndComp
$Comp
L C_Small Cld2
U 1 1 59E04B25
P 2520 6715
F 0 "Cld2" H 2530 6785 50  0000 L CNN
F 1 "4u7" H 2530 6635 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2520 6715 50  0001 C CNN
F 3 "" H 2520 6715 50  0001 C CNN
	1    2520 6715
	1    0    0    -1  
$EndComp
Wire Notes Line
	605  5830 2805 5830
Wire Notes Line
	5100 5750 5100 7700
Wire Notes Line
	4800 5750 4800 7700
Connection ~ 5950 6500
Wire Wire Line
	5950 6550 5950 6500
Wire Wire Line
	5550 6500 6350 6500
Wire Wire Line
	5550 7450 5550 6500
Wire Wire Line
	5350 7450 5550 7450
Wire Wire Line
	5200 7600 5950 7600
Wire Wire Line
	5200 7200 5200 7600
Wire Wire Line
	4700 7600 4000 7600
Wire Wire Line
	4700 7200 4700 7600
Wire Wire Line
	1470 6965 2670 6965
Wire Wire Line
	2670 6965 2670 7265
Wire Wire Line
	2670 7265 2955 7265
Connection ~ 2520 6965
Wire Wire Line
	2520 6815 2520 6965
Connection ~ 1970 6965
Wire Wire Line
	1470 6815 1470 6965
Wire Wire Line
	1970 6665 1970 7315
Wire Wire Line
	6860 1600 7110 1600
Wire Wire Line
	6860 1350 7110 1350
Wire Wire Line
	6860 1100 7110 1100
Wire Notes Line
	650  1750 3500 1750
Wire Notes Line
	3500 1750 3500 650 
Wire Notes Line
	8050 1800 10900 1800
Wire Notes Line
	8050 1800 8050 700 
Wire Wire Line
	1050 950  1650 950 
Wire Wire Line
	1050 1150 1650 1150
Wire Wire Line
	1050 1400 1650 1400
Wire Wire Line
	8950 1000 8300 1000
Wire Wire Line
	8950 1150 8300 1150
Wire Wire Line
	10400 1150 9750 1150
Wire Wire Line
	10400 1000 9750 1000
Wire Wire Line
	10400 1300 9750 1300
Wire Wire Line
	5500 4450 5500 4600
Wire Wire Line
	3600 4050 4250 4050
Wire Wire Line
	3900 4150 3900 4050
Connection ~ 3900 4050
Wire Wire Line
	3900 4450 3900 4600
Wire Wire Line
	3600 3750 3750 3750
Wire Wire Line
	4200 3750 4050 3750
Wire Wire Line
	4200 4050 4200 3750
Connection ~ 4200 4050
Wire Wire Line
	2600 3350 2600 3750
Connection ~ 2600 3350
Wire Wire Line
	2600 4050 2150 4050
Wire Wire Line
	3600 4350 3650 4350
Wire Wire Line
	4550 4050 6100 4050
Wire Wire Line
	4800 4100 4800 4050
Connection ~ 4800 4050
Wire Wire Line
	3100 5350 3100 4750
Wire Wire Line
	5500 4050 5500 4150
Wire Wire Line
	5500 5350 5500 4900
Wire Wire Line
	3900 4600 4300 4600
Wire Wire Line
	5150 4050 5150 3600
Wire Wire Line
	5150 3600 5350 3600
Connection ~ 5150 4050
Wire Wire Line
	2520 6465 2520 6615
Wire Wire Line
	1360 6465 1670 6465
Wire Wire Line
	2270 6465 3235 6465
Connection ~ 2520 6465
Wire Wire Line
	1970 7315 1770 7315
Wire Wire Line
	4550 7200 4550 7475
Wire Wire Line
	4550 7475 3975 7475
Wire Wire Line
	5350 7200 5350 7450
Wire Notes Line
	750  5550 6850 5550
Wire Notes Line
	7800 1950 7800 6400
Wire Wire Line
	5950 7600 5950 7250
Wire Wire Line
	5950 6850 5950 6950
Wire Wire Line
	1250 5350 6100 5350
Connection ~ 3100 5350
Wire Wire Line
	4800 5200 4800 5350
Connection ~ 4800 5350
Wire Wire Line
	4950 4850 4950 5050
Wire Wire Line
	3650 4350 3650 4950
Wire Wire Line
	3650 4950 4550 4950
Wire Wire Line
	6100 5350 6100 4650
Connection ~ 5500 5350
Wire Wire Line
	6100 4050 6100 4350
Connection ~ 5500 4050
Wire Wire Line
	2600 4350 2350 4350
Wire Wire Line
	2350 4350 2350 5250
Wire Wire Line
	2350 5250 2600 5250
Wire Wire Line
	8950 1300 8300 1300
Wire Wire Line
	1350 3350 1350 3900
Connection ~ 1350 3350
Wire Wire Line
	1350 4200 1350 5350
Connection ~ 1350 5350
Wire Wire Line
	1750 3350 1750 3900
Connection ~ 1750 3350
Wire Wire Line
	1750 5350 1750 4200
Connection ~ 1750 5350
Wire Wire Line
	1470 6465 1470 6615
Connection ~ 1470 6465
Wire Wire Line
	2955 6865 2955 6965
Wire Wire Line
	1200 3350 3100 3350
$Comp
L Q_NPN_BCE Qln1
U 1 1 5ABDADD3
P 9625 4450
F 0 "Qln1" H 10000 4500 50  0000 R CNN
F 1 "BUX85" H 10050 4375 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical_LargePads" H 9825 4550 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/BUX85-D.PDF" H 9625 4450 50  0001 C CNN
F 4 "??" H 9625 4450 60  0001 C CNN "Ref. RS"
	1    9625 4450
	1    0    0    -1  
$EndComp
$Comp
L R Rln2
U 1 1 5ABDAF2D
P 9325 4075
F 0 "Rln2" V 9405 4075 50  0000 C CNN
F 1 "150k" V 9325 4075 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9255 4075 50  0001 C CNN
F 3 "" H 9325 4075 50  0000 C CNN
	1    9325 4075
	1    0    0    -1  
$EndComp
$Comp
L ZENER Dln1
U 1 1 5ABDB116
P 9325 5025
F 0 "Dln1" H 9325 5125 50  0000 C CNN
F 1 "MMSZ15T1G" H 9325 4925 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 9325 5025 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/14af/0900766b814af7dd.pdf" H 9325 5025 50  0001 C CNN
F 4 "MMSZ15T1G" H 9325 5025 60  0001 C CNN "Component"
F 5 "544-9810" H 9325 5025 60  0001 C CNN "Ref. RS"
	1    9325 5025
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 3800 9725 3800
Wire Wire Line
	9325 3800 9325 3925
Wire Wire Line
	9325 4225 9325 4825
Wire Wire Line
	9425 4450 9325 4450
Connection ~ 9325 4450
Wire Wire Line
	9725 3800 9725 4250
Connection ~ 9325 3800
Wire Wire Line
	9725 4650 9725 4900
Wire Wire Line
	9325 5225 9325 5600
Text Label 10425 4750 0    60   ~ 0
+15VHigh
Wire Wire Line
	10425 4750 9725 4750
Connection ~ 9725 4750
Wire Wire Line
	9725 5200 9725 5600
Text Label 9725 5600 3    60   ~ 0
+15VLMR
Text Label 1250 5350 2    60   ~ 0
GNDPwR
Text Label 9325 5600 3    60   ~ 0
GNDPwR
Text Notes 8850 2225 0    60   ~ 0
BLACK START LINEAR FEEDER
Text Notes 8200 2325 0    60   ~ 0
----------------------------
Text Notes 8175 3050 0    60   ~ 0
This linear feeder drivers energy from the +120V\nin the case of a black start from the high voltage level.\nIt generates +14.8V from the 120V with a very low efficiency\nallowing the system to start-up before the +15.8V from the LMR \ncan take over the feeding of the board.\n75k - 1/4 W (2x 150k - 1/8 W in parallel)\nZener - MMSZ15T1G - 500mW, SOD123\nSchottky - MBR2H100SFT3G - 100V, 2A, SOD123
$Comp
L CONN_01X01 Tf4
U 1 1 5ABEB770
P 6660 1875
F 0 "Tf4" H 6660 1975 50  0000 C CNN
F 1 "CON1X1" V 6760 1925 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6660 1875 50  0001 C CNN
F 3 "" H 6660 1875 50  0000 C CNN
	1    6660 1875
	-1   0    0    1   
$EndComp
Text Label 7110 1875 0    60   ~ 0
GNDLow
Wire Wire Line
	6860 1875 7110 1875
$Comp
L CONN_01X01 Tf5
U 1 1 5ABEBB27
P 6660 2150
F 0 "Tf5" H 6660 2250 50  0000 C CNN
F 1 "CON1X1" V 6760 2200 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6660 2150 50  0001 C CNN
F 3 "" H 6660 2150 50  0000 C CNN
	1    6660 2150
	-1   0    0    1   
$EndComp
Text Label 7110 2150 0    60   ~ 0
GNDPwR
Wire Wire Line
	6860 2150 7110 2150
$Comp
L R Rld1
U 1 1 5ABEB100
P 1210 6465
F 0 "Rld1" V 1310 6465 50  0000 C CNN
F 1 "22" V 1210 6465 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1140 6465 50  0001 C CNN
F 3 "" H 1210 6465 50  0000 C CNN
F 4 "??" V 1210 6465 60  0001 C CNN "RS ref."
	1    1210 6465
	0    1    1    0   
$EndComp
Wire Wire Line
	2955 6565 2955 6465
Connection ~ 2955 6465
Wire Wire Line
	650  6465 1060 6465
Text Notes 595  6150 0    60   ~ 0
This linear regulator has a low drop out and can take 16.5V input.\nA 22 ohm, 1/4W resistance is added in series to regulate input voltage\nand to serve as a fuse in case of overpower
$Comp
L BA50BC0FP-E2 Uf2
U 1 1 5AC493E3
P 1970 6465
F 0 "Uf2" H 2070 6315 50  0000 C CNN
F 1 "BA50BC0FP-E2" H 1970 6615 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2Lead" H 1970 6465 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/161b/0900766b8161bc23.pdf" H 1970 6465 50  0001 C CNN
F 4 "167-7335" H 1970 6465 60  0001 C CNN "RS ref."
	1    1970 6465
	1    0    0    -1  
$EndComp
Text Notes 6585 770  0    60   ~ 0
FEEDER TEST POINTS
Text Notes 6565 920  0    60   ~ 0
--------------
Wire Wire Line
	4800 4400 4800 4900
Wire Wire Line
	4950 4850 4800 4850
Connection ~ 4800 4850
Wire Wire Line
	4550 4950 4550 4650
Wire Wire Line
	4550 4650 4800 4650
Connection ~ 4800 4650
$Comp
L R Rt3
U 1 1 5AC4F040
P 2150 4325
F 0 "Rt3" V 2230 4325 50  0000 C CNN
F 1 "10k" V 2150 4325 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2080 4325 50  0001 C CNN
F 3 "" H 2150 4325 50  0000 C CNN
	1    2150 4325
	1    0    0    -1  
$EndComp
$Comp
L R Rt1
U 1 1 5AC4F0C9
P 2150 5025
F 0 "Rt1" V 2230 5025 50  0000 C CNN
F 1 "10k" V 2150 5025 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2080 5025 50  0001 C CNN
F 3 "" H 2150 5025 50  0000 C CNN
	1    2150 5025
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4050 2150 4175
Wire Wire Line
	2150 4525 2150 4475
Wire Wire Line
	2150 4825 2150 4875
Wire Wire Line
	2150 5175 2150 5350
Connection ~ 2150 5350
$Comp
L R Rpu1
U 1 1 5AC4FE4C
P 2475 5025
F 0 "Rpu1" V 2555 5025 50  0000 C CNN
F 1 "10k" V 2475 5025 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2405 5025 50  0001 C CNN
F 3 "" H 2475 5025 50  0000 C CNN
	1    2475 5025
	1    0    0    -1  
$EndComp
Wire Wire Line
	2475 5175 2475 5250
Connection ~ 2475 5250
Wire Wire Line
	2475 4875 2475 4800
Text Label 2475 4800 0    60   ~ 0
+5VHigh
$Comp
L D_Schottky Dln2
U 1 1 5AC52F51
P 9725 5050
F 0 "Dln2" H 9725 5150 50  0000 C CNN
F 1 "MBR2H100SFT3G" H 9725 4950 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 9725 5050 50  0001 C CNN
F 3 "https://docs-emea.rs-online.com/webdocs/1221/0900766b812212a8.pdf" H 9725 5050 50  0001 C CNN
F 4 "781-5650" H 9725 5050 60  0001 C CNN "Ref. RS"
	1    9725 5050
	0    1    1    0   
$EndComp
Text Notes 3775 3400 0    60   ~ 0
DC-DC converter Design Notes\n------------------------------\nVin 17V to 60V // Vout 15.8V, 1A\n---INDUCTOR AND CURRENT---\nInductor = 12LRS333C (Murata) (or higher)\nDiL = 0.5A\n---SETUP-ELEMENTS---\nFB = 0.75V (Potentiometer for bettter precision)\nRT = 30k (3x10k) (for an operation around 700kHz)\nRpg = 10k (pull-up which sends the "power good" signal to the Arduino)\n--CAPACITORS-VOLTAGE----\nCH1 = 20u (should hold up to 100V, thin-film) \nCH2 = 4.7u (should hold up to 100V, ceramic)\nCH3 = 20u (150% bigger than the minimum)\nCb = 100n (ceramic, X7R, for good thermal stability)\n--SCHOTTKY DIODE--\nMBR2H100SFT3G - 100V, 2A, SOD123
Text Notes 725  2425 0    60   ~ 0
This buck converter can take a maximum input of 60VDC\nto produce 15VDC. It is used to feed all of the power module.
Text Notes 775  2100 0    60   ~ 0
DC-DC CONVERTER
Text Notes 775  2200 0    60   ~ 0
-----------------------------
$Comp
L R Rln1
U 1 1 5AC5594A
P 9150 4075
F 0 "Rln1" V 9230 4075 50  0000 C CNN
F 1 "150k" V 9150 4075 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 9080 4075 50  0001 C CNN
F 3 "" H 9150 4075 50  0000 C CNN
	1    9150 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3925 9150 3850
Wire Wire Line
	9150 3850 9325 3850
Connection ~ 9325 3850
Wire Wire Line
	9150 4225 9150 4325
Wire Wire Line
	9150 4325 9325 4325
Connection ~ 9325 4325
Wire Wire Line
	650  6465 650  6680
Wire Wire Line
	650  6680 710  6680
Text Notes 7375 7500 0    60   ~ 0
FEEDER BLOCK
$EndSCHEMATC
