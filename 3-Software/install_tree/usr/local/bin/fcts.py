#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  fcts.py
#
#  Copyright 2018 hugues <hugues.larrive@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from __future__ import print_function
def printf(str, *args):
    print(str % args, end='')

import struct
import binascii

def from_bytes(b, byteorder='big'):
	# type: (bytes, str) -> Int
	val = bytes(memoryview(b).tobytes())
	byteorder = byteorder.lower()
	ch = '>' if byteorder.startswith('b') else '<'
	hi = struct.unpack(ch + 'B'*len(val), val)
	xo = 0 + sum(n**(i+1) for i, n in enumerate(hi))
	return xo

def to_bytes(integer, length, byteorder='big'):
	try:
		ch = '>' if byteorder.startswith('b') else '<'
		hex_str = '%x' % int(integer)
		n = len(hex_str)
		x = binascii.unhexlify(hex_str.zfill(n + (n & 1)))
		val = x[-1 * length:] if ch == '>' else x[:length]
	except OverflowError:
		raise ValueError(integer)
	return val


