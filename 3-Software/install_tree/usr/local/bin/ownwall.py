#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  ownwall.py
#  
#  Copyright 2018 hugues <hugues.larrive@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import serial  	# library for serial communication
import binascii	# hexadecimal to binary conversion
import sys		# for sys.argv
import os		# for chmod

from datetime import datetime
from fcts import printf,from_bytes,to_bytes

# writing json setup file function
def json_setup(port, board_id, duty_cycle, mesure_cha, setting,
tracking_on, pwm_run, vmil_cal, vmil_offset, vmih_cal, vmih_offset,
vml_cal, vmh_cal, vmt1_cal):
	setup = open('/var/local/ownwall/'+port+'_setup.json', 'w')
	setup.write('{')
	setup.write('"board_id":'+str(board_id)+',')
	setup.write('"duty_cycle":'+str(duty_cycle)+',')
	setup.write('"mesure_cha":'+str(mesure_cha)+',')
	setup.write('"setting":'+str(setting)+',')
	setup.write('"tracking_on":'+str(tracking_on)+',')
	setup.write('"pwm_run":'+str(pwm_run)+',')
	setup.write('"vmil_cal":'+str(vmil_cal)+',')
	setup.write('"vmil_offset":'+str(vmil_offset)+',')
	setup.write('"vmih_cal":'+str(vmih_cal)+',')
	setup.write('"vmih_offset":'+str(vmih_offset)+',')
	setup.write('"vml_cal":'+str(vml_cal)+',')
	setup.write('"vmh_cal":'+str(vmh_cal)+',')
	setup.write('"vmt1_cal":'+str(vmt1_cal))
	setup.write('}')
	setup.close()




quiet = 0

# reading command line arguments
if(len(sys.argv)>1): port = sys.argv[1]
else:
	port = "ttyUSB0"
	print "OwnWall : No port provided, default to ttyUSB0"
print "OwnWall : Starting ownwall capture from "+port+" serial device"  
if(len(sys.argv)>2 and sys.argv[2]=='-q'): quiet = 1

# initializing serial communication (this will reset the nano)
ser0 = serial.Serial(
	port="/dev/"+port,
	baudrate=57600,
	bytesize=serial.EIGHTBITS,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	timeout=None)

# Calibration :
# Vref = 2,5V, 12 bits -> 2003 pas de 0,0012481 V
# VimL,VimH = 0,10V/A -> 0,10/0,0012481 = 80.12
# VmL = 0,0833333 -> 0,0833333/0,0012481 = 66.97
# VmH = 0,0416667 -> 0,0416667/0,0012481 = 33.48
# VmT1 = 10mV/°C -> 0,01/0,0012481 = 8.06
cha_name = 	[	"VmiL",	"VmiH",	"VmL",	"VmH",	"VmT1",	"Pgood","PWM"]
cha_cal = 	[	105.3,	105.3,	53.57,	26.6,	8.19,	1,	 	6.4]
cha_offset = [	2068,	2068,	0,		0,		0,		0,		0]	
cha_unit = [	"A",	"A",	"V",	"V",	"°C",	"raw",	"%"]
int_mesure = [0,0,0,0,0,0,0]
int_raw_mes = [0,0,0,0,0,0,0]
int_avg_mes = [0,0,0,0,0,0,0]
int_raw_avg_mes = [0,0,0,0,0,0,0]
avg_counter = 0
avg_number = 10 # number of mesures to average
cmd = ""


# reading board setup from serial
board_id = from_bytes(ser0.read())
duty_cycle_high_byte = from_bytes(ser0.read())
duty_cycle_low_byte = from_bytes(ser0.read())
mesure_cha = from_bytes(ser0.read())
setting_high_byte = from_bytes(ser0.read())
setting_low_byte = from_bytes(ser0.read())
tracking_on = from_bytes(ser0.read())
pwm_run = from_bytes(ser0.read())
vmil_cal_high_byte = from_bytes(ser0.read())
vmil_cal_low_byte = from_bytes(ser0.read())
vmil_offset_high_byte = from_bytes(ser0.read())
vmil_offset_low_byte = from_bytes(ser0.read())
vmih_cal_high_byte = from_bytes(ser0.read())
vmih_cal_low_byte = from_bytes(ser0.read())
vmih_offset_high_byte = from_bytes(ser0.read())
vmih_offset_low_byte = from_bytes(ser0.read())
vml_cal_high_byte = from_bytes(ser0.read())
vml_cal_low_byte = from_bytes(ser0.read())
vmh_cal_high_byte = from_bytes(ser0.read())
vmh_cal_low_byte = from_bytes(ser0.read())
vmt1_cal_high_byte = from_bytes(ser0.read())
vmt1_cal_low_byte = from_bytes(ser0.read())
# computing words
duty_cycle = duty_cycle_high_byte*256 + duty_cycle_low_byte
setting = setting_high_byte*256 + setting_low_byte
vmil_cal = vmil_cal_high_byte*256 + vmil_cal_low_byte
vmil_offset = vmil_offset_high_byte*256 + vmil_offset_low_byte
vmih_cal = vmih_cal_high_byte*256 + vmih_cal_low_byte
vmih_offset = vmih_offset_high_byte*256 + vmih_offset_low_byte
vml_cal = vml_cal_high_byte*256 + vml_cal_low_byte
vmh_cal = vmh_cal_high_byte*256 + vmh_cal_low_byte
vmt1_cal = vmt1_cal_high_byte*256 + vmt1_cal_low_byte

# Setting calibers and offsets
cha_cal[0] = vmil_cal/100.0;
cha_offset[0] = vmil_offset;
cha_cal[1] = vmih_cal/100.0;
cha_offset[1] = vmih_offset;
cha_cal[2] = vml_cal/100.0;
cha_cal[3] = vmh_cal/100.0;
cha_cal[4] = vmt1_cal/100.0;

# If caliber==0 default to 1
for i in range(0,5):
	if cha_cal[i]==0: cha_cal[i]=1; 

# printing setup to stdout
if not quiet:
	print """
	***************
	* Board setup *
	***************
	"""

	print "Board ID: "+str(board_id)

	print "Duty-cycle = "+str(duty_cycle)

	print "Tracking channel is",
	if mesure_cha < len(cha_name): print cha_name[mesure_cha]
	else: print "not defined"

	print "Tracked value = "+str(setting)

	print "Tracking is",
	if tracking_on: print "on"
	else: print "off"

	print "PWM state is",
	if pwm_run: print "running"
	else: print "stopped"

	print "vmil_cal = "+str(vmil_cal)
	print "vmil_offset = "+str(vmil_offset)
	print "vmih_cal = "+str(vmih_cal)
	print "vmih_offset = "+str(vmih_offset)
	print "vml_cal = "+str(vml_cal)
	print "vmh_cal = "+str(vmh_cal)
	print "vmt1_cal = "+str(vmt1_cal)

	print 

# writing json setup file
json_setup(port, board_id, duty_cycle, mesure_cha, setting,
tracking_on, pwm_run, vmil_cal, vmil_offset, vmih_cal, vmih_offset,
vml_cal, vmh_cal, vmt1_cal)

# writing output file header
capture_date = str(datetime.now())[0:19].replace(' ','_').replace('-','').replace(':','')
time = .0
out = open('/var/local/ownwall/capture_'+str(board_id)+'_'+capture_date+'.out','w')
outstr= "#Time"
for i in range(0,7):
	outstr += " "+cha_name[i]
out.write(outstr+"\n"+"0 0 0 0 0 0 0 0\n")


# create command file
command = open('/var/local/ownwall/'+port+'_cmd','w')
command.close()
os.chmod('/var/local/ownwall/'+port+'_cmd',0666)

while 1:
	# reading 7 mesures from serial + 2 dummy bytes
	i=0
	for byte in ser0.read(16):
		if i<14:
			if i%2==0: 	# first we read bits[15:8]
				high_byte = from_bytes(byte, byteorder='big')
			else:		# then we read bits[7:0]
				low_byte = from_bytes(byte, byteorder='big')
						# we make a word from this two bytes and we calulate the float value
				int_raw_mes[i/2] = high_byte*256 + low_byte
				int_raw_avg_mes[i/2] += int_raw_mes[i/2]
				int_mesure[i/2] = (int_raw_mes[i/2] - cha_offset[i/2])/cha_cal[i/2]
				int_avg_mes[i/2] += int_mesure[i/2]
		i += 1

	# displaying mesures in terminal and writing it to json mesures file
	str_mes = "\r"
	json_mes = "{"
	avg_counter += 1
	if avg_counter==avg_number:
		avg_counter = 0
		for j in range(0,7):
			int_avg_mes[j] /= avg_number
			int_raw_avg_mes[j] /= avg_number
			spaces = ""
			#if int_avg_mes[j] < 1000: spaces += " " # will always occure
			if int_avg_mes[j] < 100: spaces += " "
			if int_avg_mes[j] < 10: spaces += " "
			if int_avg_mes[j] >=0: spaces += " "
			if int_raw_avg_mes[j] < 1000: spaces += " "
			if int_raw_avg_mes[j] < 100: spaces += " "
			if int_raw_avg_mes[j] < 10: spaces += " "
			str_mes += "%s=%.2f%s (%d) "+spaces
			json_mes += '"'+cha_name[j]+'":'+str(int(int_avg_mes[j]*100)/100.0)
			if j<6: json_mes += ','
		json_mes += '}'
		json = open('/var/local/ownwall/'+port+'_mesures.json', 'w')
		json.write(json_mes)
		json.close()
		# now we print it
		if not quiet:
			printf(str_mes+cmd,
				cha_name[0], int_avg_mes[0], cha_unit[0], int_raw_avg_mes[0],
				cha_name[1], int_avg_mes[1], cha_unit[1], int_raw_avg_mes[1],
				cha_name[2], int_avg_mes[2], cha_unit[2], int_raw_avg_mes[2],
				cha_name[3], int_avg_mes[3], cha_unit[3], int_raw_avg_mes[3],
				cha_name[4], int_avg_mes[4], cha_unit[4], int_raw_avg_mes[4],
				cha_name[5], int_avg_mes[5], cha_unit[5], int_raw_avg_mes[5],
				cha_name[6], int_avg_mes[6], cha_unit[6], int_raw_avg_mes[6]
			)
		for j in range(0,7):
			int_avg_mes[j] = 0
			int_raw_avg_mes[j] = 0
		
	
	# Writing mesures to capture file
	time += .00384
	out.write(str(time)+" "+str(int_mesure[0])+" "+str(int_mesure[1])
	+" "+str(int_mesure[2])+" "+str(int_mesure[3])+" "
	+str(int_mesure[4])+" "+str(int_mesure[5])+" "
	+str(int_mesure[6])+"\n")
	
	# reading command file
	cmdfile = open('/var/local/ownwall/'+port+'_cmd','r')
	cmdvar = cmdfile.read()
	if(cmdvar):
		cmdfile.close()
		cmdfile = open('/var/local/ownwall/'+port+'_cmd','w') # this delete file content
		argvs = cmdvar.split(' ')
		variable = argvs[0]
		
		# set uint8_t board_id
		if(variable=='board_id'):
			board_id = int(argvs[1])
			ser0.write('b')
			ser0.write(to_bytes(board_id, 1))
		
		# set uint16_t duty_cycle
		if(variable=='duty_cycle'):
			duty_cycle = int(argvs[1])
			dc_high_byte = to_bytes(int(argvs[1])>>8, 1)
			dc_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('d')
			ser0.write(dc_high_byte)
			ser0.write(dc_low_byte)
		
		# set uint8_t mesure_cha
		if(variable=='mesure_cha'):
			mesure_cha = int(argvs[1])
			ser0.write('m')
			ser0.write(to_bytes(int(argvs[1]), 1))
		
		# set uint16_t setting
		if(variable=='setting'):
			setting = int(argvs[1])
			s_high_byte = to_bytes(int(argvs[1])>>8, 1)
			s_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('s')
			ser0.write(s_high_byte)
			ser0.write(s_low_byte)
		
		# set uint8_t tracking_on
		if(variable=='tracking_on'):
			tracking_on = int(argvs[1])
			ser0.write('t')
			ser0.write(to_bytes(int(argvs[1]), 1))
		
		# set uint8_t pwm_run
		if(variable=='pwm_run'):
			pwm_run = int(argvs[1])
			ser0.write('p')
			ser0.write(to_bytes(int(argvs[1]), 1))
		
		# set uint16_t vmil_cal
		if(variable=='vmil_cal'):
			vmil_cal = int(argvs[1])
			cha_cal[0] = vmil_cal/100.0;
			e_high_byte = to_bytes(int(argvs[1])>>8, 1)
			e_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('e')
			ser0.write(e_high_byte)
			ser0.write(e_low_byte)
		
		# set uint16_t vmil_offset
		if(variable=='vmil_offset'):
			vmil_offset = int(argvs[1])
			cha_offset[0] = vmil_offset;
			f_high_byte = to_bytes(int(argvs[1])>>8, 1)
			f_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('f')
			ser0.write(f_high_byte)
			ser0.write(f_low_byte)
		
		# set uint16_t vmih_cal
		if(variable=='vmih_cal'):
			vmih_cal = int(argvs[1])
			cha_cal[1] = vmih_cal/100.0;
			g_high_byte = to_bytes(int(argvs[1])>>8, 1)
			g_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('g')
			ser0.write(g_high_byte)
			ser0.write(g_low_byte)
		
		# set uint16_t vmih_offset
		if(variable=='vmih_offset'):
			vmih_offset = int(argvs[1])
			cha_offset[1] = vmih_offset;
			h_high_byte = to_bytes(int(argvs[1])>>8, 1)
			h_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('h')
			ser0.write(h_high_byte)
			ser0.write(h_low_byte)
		
		# set uint16_t vml_cal
		if(variable=='vml_cal'):
			vml_cal = int(argvs[1])
			cha_cal[2] = vml_cal/100.0;
			i_high_byte = to_bytes(int(argvs[1])>>8, 1)
			i_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('i')
			ser0.write(i_high_byte)
			ser0.write(i_low_byte)
		
		# set uint16_t vmh_cal
		if(variable=='vmh_cal'):
			vmh_cal = int(argvs[1])
			cha_cal[3] = vmh_cal/100.0;
			j_high_byte = to_bytes(int(argvs[1])>>8, 1)
			j_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('j')
			ser0.write(j_high_byte)
			ser0.write(j_low_byte)
		
		# set uint16_t vmt1_cal
		if(variable=='vmt1_cal'):
			vmt1_cal = int(argvs[1])
			cha_cal[4] = vmt1_cal/100.0;
			k_high_byte = to_bytes(int(argvs[1])>>8, 1)
			k_low_byte = to_bytes(int(argvs[1])&0x00ff, 1)
			ser0.write('k')
			ser0.write(k_high_byte)
			ser0.write(k_low_byte)
				
		# writing json setup file
		json_setup(port, board_id, duty_cycle, mesure_cha, setting,
		tracking_on, pwm_run, vmil_cal, vmil_offset, vmih_cal, vmih_offset,
		vml_cal, vmh_cal, vmt1_cal)
	cmdfile.close()
