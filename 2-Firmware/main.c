/*
 * OwnWall main.c 
 * 
 *  Created on: 12 feb. 2018
 * 
 * Copyright 2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <stdlib.h>
#include "pwm.h"
#include "rs232.h" 
#include <avr/eeprom.h>
#include "daq.h"
#include "spi.h"


/** global vars for pwm interrupt */
extern volatile uint8_t pwm_int[8];
extern volatile uint8_t pwm_idx;
extern volatile uint8_t timer_10us;
extern volatile uint8_t pwm_run;
extern volatile uint8_t pwm_real_dt; // real values of duty cycle for OCRnB
extern volatile uint8_t pwm_real_dt_higher; // pwm_real_dt + 1
extern volatile uint8_t pwm_mod; // modulo
	

/** global vars for rs232 interrupts */
extern volatile uint8_t rs232_rx_buffer[4];
extern volatile uint8_t rs232_rx_idx;
extern volatile uint8_t rs232_rx_flag;
extern volatile uint8_t rs232_tx_buffer[50];
extern volatile uint8_t rs232_tx_idx;
extern volatile uint8_t rs232_tx_flag;
extern volatile uint8_t rs232_tx_len;

/** global var for spi interrupts */
extern volatile uint8_t spi_buffer[3];

int main(void)
{
	/** rs232 vars */
	uint8_t rs232_fwd_ct = 0;
	uint8_t rs232_tx_fill_idx = 0;
	uint8_t rs232_tx_trig = 0;
	
	/** daq vars */
	uint16_t daq_accu[6] = {0};
	uint8_t	daq_accu_idx = 0, daq_prev_idx = 0;
	uint8_t daq_avg_ct = 0, daq_avg_idx = 0;
	uint8_t daq_avg_hight = 0, daq_avg_low = 0;
	
	/** realtime loop var */
	uint8_t height_step = 0;
	
	/** voltage tracking vars*/
	uint16_t mesure = 0xfff;

	/** Setup vars */
	uint8_t		board_id;		// an identifier for the board
	uint16_t	duty_cycle;		// initial duty_cycle
	uint8_t		mesure_cha; 	// channel of ADC to track on (2=VmL)
	uint16_t	setting;		// value tracked for mesure_cha (12*53,57)
	uint8_t		tracking_on;	// toggle tracking on or off

	/** initialisation */
	rs232_init(B57600, D8, PN, STP1);
	pwm_init();
	spi_init();
	sei();	

	/** reading setup from eeprom */
	board_id = eeprom_read_byte((uint8_t*)0);
	duty_cycle = eeprom_read_byte((uint8_t*)1)*256 + eeprom_read_byte((uint8_t*)2);
	mesure_cha = eeprom_read_byte((uint8_t*)3);
	setting = eeprom_read_byte((uint8_t*)4)*256 + eeprom_read_byte((uint8_t*)5);
	tracking_on = eeprom_read_byte((uint8_t*)6);
	pwm_run = eeprom_read_byte((uint8_t*)7);
	
	/** sending setup to software */
	rs232_tx_buffer[0] = board_id;
	rs232_tx_buffer[1] = (uint8_t)(duty_cycle>>8);
	rs232_tx_buffer[2] = (uint8_t)duty_cycle;
	rs232_tx_buffer[3] = mesure_cha;
	rs232_tx_buffer[4] = (uint8_t)(setting>>8);
	rs232_tx_buffer[5] = (uint8_t)setting;
	rs232_tx_buffer[6] = tracking_on;
	rs232_tx_buffer[7] = pwm_run;
	// copy calibration values
	for(uint16_t i=8; i<22; i++)
		rs232_tx_buffer[i] = eeprom_read_byte((uint8_t*)i);
	rs232_tx_len = 22;
	RS232_TX; // launch transmission
	while(rs232_tx_flag); // wait for transmission end
	UCSR0B &= ~(1 << TXCIE0); // disable TX interrupt

	DDRC = 0xff; // for debugging
		
	while(1)
	{
		/** Mesures */
		if(timer_10us) // every 10us (step time)
		{
			//PORTC |= 1;
			
			timer_10us = 0; // reset the flag
			
			switch(height_step)
			{
			case 0:
				//PORTC |= 1;
				
				/** Feedback step 5 */
				mesure = DAQ_CATCH; // Catch a mesure for tracking
				//* duty cycle correction step 1 */
				if(tracking_on
				&& (mesure < setting)
				&& (duty_cycle < 639)) duty_cycle++;
				/** End feedback step 5 (1,8µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ setp 1 */
				DAQ_LAUNCH(daq_accu_idx); // Launch mesure for DAQ				
				//* loop over the six channels every 480µs */
				daq_prev_idx = daq_accu_idx;
				daq_accu_idx++;
				if(daq_accu_idx == 6) daq_accu_idx = 0;
				/** End DAQ step 1 (1,8µs to 2µs) */

				//PORTC &= ~1;
				//* 3,4µs to 3,6µs from case 0
								
				height_step = 1;
				break;
				
			case 1:
				//PORTC |= 1;
				
				/** Feedback step 6 */
				//** duty cycle correction step 2 */
				if(tracking_on)
				{
					if(mesure > setting && duty_cycle > 0) duty_cycle--;
					if(mesure==0) duty_cycle = 0;
					//last_mesure = mesure;
				}
				//** setting new pwm duty cycle step 1 */
				PWM_SET1;
				/** End feedback step 6 (2,5µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 2 */
				SPI_2; // writing the second SPI command byte
				/** End DAQ step 2 (0,3µs) */
				
				
				//PORTC &= ~1;
				//* 2,7µs from case 1
								
				height_step = 2;
				break;
				
			case 2:
				//PORTC |= 1;
				
				/** Feedback step 7 */
				//** setting new pwm duty cycle step 2 */
				PWM_SET2;
				/** End feedback step 7 (2,6µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 3 */
				SPI_3;
				/** End DAQ step 3 (0,3µs) */
				
				//PORTC &= ~1;
				//* 2,8µs from case 2
				
				height_step = 3;
				break;
				
			case 3:
				//PORTC |= 1;
				
				/** Feedback step 8 */
				PWM_SET3;
				/** End feedback step 8 (3,2µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 4 */
				SPI_END;
				/** End DAQ step 4 (0,4µs) */
				
				//PORTC &= ~1;
				//* 3,5µs from case 3
				
				height_step = 4;
				break;
				
			case 4:
				//PORTC |= 1;
				
				/** DAQ step 5 */
				daq_accu[daq_prev_idx] += DAQ_CATCH;
				/** End DAQ step 5 (1,7µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** Feedback step 1 */
				DAQ_LAUNCH(mesure_cha);
				/** End feedback step 1 (1,4µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 5 bis (delayed for DAQ_LAUNCH regularity) */
				if(daq_prev_idx == 5) daq_avg_ct++;
				/** End DAQ step 5 bis (0,3µs to 0,6µs) */

				//PORTC &= ~1;
				//* 3,3µs to 3,5µs from case 4
				
				height_step = 5;
				break;
				
			case 5:
				//PORTC |= 1;
				
				/** Feedback step 9 */
				PWM_SET4;
				/** End feedback step 9 (0,8µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** Feedback step 2 */
				SPI_2;
				/** End feedback step 2 (0,3µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** DAQ step 6 (every 3840µs) */
				//* Averaging 8 mesures per channel
				if(daq_avg_ct == 8)
				{
					//* >> 3 to divide by 8, 8+3=11 for msb
					daq_avg_hight = (uint8_t)(daq_accu[daq_avg_idx] >> 11);
					daq_avg_low = (uint8_t)(daq_accu[daq_avg_idx] >> 3);
					daq_accu[daq_avg_idx] = 0; // reset accumulator
					
				}
				/** End DAQ step 6 (0,3µ or 2,3µs) */
				
				//PORTC &= ~1;
				//* 1,2µs or 3,2µs from case 5
				
				height_step = 6;
				break;
				
			case 6:
				//PORTC |= 1;

				/** DAQ step 7 */
				//** transfert raw mesure data into TX buffer */
				if(daq_avg_ct == 8)
				{
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_hight;
					rs232_tx_buffer[rs232_tx_fill_idx++] = daq_avg_low;
					
					if(daq_avg_idx == 5) // all mesures where buffered
					{
						rs232_tx_fill_idx = 0;
						
						/* buffering current PWM value */
						rs232_tx_buffer[12] = duty_cycle>>8;
						rs232_tx_buffer[13] = (uint8_t)(duty_cycle&0x00ff);
						rs232_tx_trig = 1;
						
						daq_avg_idx = 0; // reset index
						daq_avg_ct = 0; // reset avg counter
					}
					else daq_avg_idx++; // increment index
				}
				/** End DAQ step 7 (0,3µs / 2,2µs / 2,6µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** Feedback step 3 */
				SPI_3;
				/** End feedback step 3 (0,4µs) */
				
				//PORTC &= ~1;
				//* 0,6µs / 2,5µs / 2,8µs from case 6
				
				height_step = 7;	
				break;
				
			case 7:
				//PORTC |= 1;
				
				/** DAQ step 8 */
				//** forward serial transfert **/
				if(rs232_tx_flag && ++rs232_fwd_ct==3)
				{
					rs232_fwd_ct=0;
					RS232_FWD;
				}
				//** or launch new serial transfert **/
				if(rs232_tx_trig)
				{
						rs232_tx_trig = 0;
						rs232_tx_len = 16;
						RS232_TX;
				}
				/** End DAQ step 8 (0,6µs / 0,9µs / 2,4µs / 2,8µs) */
				
				//PORTC &= ~1;
				//PORTC |= 1;
				
				/** Feedback step 4 */
				SPI_END;
				/** End feedback step 4 (0,5µs) */
				
				//PORTC &= ~1;
				//* 1µs to 3,1µs from case 7
				
				height_step = 0;
				break;
			} // end of switch(height_step)
		} // end if(timer_10us)		
		
		/** receive settings from rs232 */
		if(rs232_rx_flag)
		{
			rs232_rx_flag = 0;	// reset rx flag

			switch(rs232_rx_buffer[0])
			{	
				case 'b': // set board_id
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)0,rs232_rx_buffer[1]);
					}
					break;
				
				case 'd': // set duty_cycle
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)1,rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)2,rs232_rx_buffer[2]);
						duty_cycle = rs232_rx_buffer[1]*256+rs232_rx_buffer[2];
						PWM_SET1;
						PWM_SET2;
						PWM_SET3;
						PWM_SET4;
					}
					break;
				
				case 'm': // set mesure_cha
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						mesure_cha = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)3, mesure_cha);
					}
					break;
				
				case 's': // set setting
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						setting = rs232_rx_buffer[1]*256 + rs232_rx_buffer[2];
						eeprom_write_byte((uint8_t*)4, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)5, rs232_rx_buffer[2]);
					}
					break;
				
				case 't': // set tracking_on
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						tracking_on = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)6, tracking_on);
						// reset duty_cycle
						if(!tracking_on)
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
					}
					break;
				
				case 'p': // set pwm_run
					if(rs232_rx_idx==2)
					{
						rs232_rx_idx = 0;
						pwm_run = rs232_rx_buffer[1];
						eeprom_write_byte((uint8_t*)7, pwm_run);
						if(pwm_run)
						{
							duty_cycle =
								eeprom_read_byte((uint8_t*)1)*256
								+ eeprom_read_byte((uint8_t*)2);
							PWM_SET1;
							PWM_SET2;
							PWM_SET3;
							PWM_SET4;
						}
						else pwm_stop();
					}
					break;				

				case 'e': // set vmil_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)8, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)9, rs232_rx_buffer[2]);
					}
					break;
				
				case 'f': // set vmil_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)10, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)11, rs232_rx_buffer[2]);
					}
					break;
				
				case 'g': // set vmih_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)12, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)13, rs232_rx_buffer[2]);
					}
					break;
				
				case 'h': // set vmih_offset
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)14, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)15, rs232_rx_buffer[2]);
					}
					break;
				
				case 'i': // set vml_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)16, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)17, rs232_rx_buffer[2]);
					}
					break;
				
				case 'j': // set vmh_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)18, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)19, rs232_rx_buffer[2]);
					}
					break;
				
				case 'k': // set vmt1_cal
					if(rs232_rx_idx==3)
					{
						rs232_rx_idx = 0;
						eeprom_write_byte((uint8_t*)20, rs232_rx_buffer[1]);
						eeprom_write_byte((uint8_t*)21, rs232_rx_buffer[2]);
					}
					break;
				
			}
			// end switch(rs232_rx_buffer[0])			
		} 
		// end if(rs232_rx_flag)
	}
	// end while(1)
}
// end main()
