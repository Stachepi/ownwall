/*
 * rs232.h
 *
 * Modified OwnWall version
 *
 *  Created on: 10 oct. 2017
 * 
 * Copyright 2017-2018 hugues <hugues.larrive@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef RS232_H_
#define RS232_H_

#include <avr/interrupt.h>

/*
volatile bool flag_IT_RX;
volatile char data;
*/
volatile uint8_t	rs232_rx_buffer[4];
volatile uint8_t 	rs232_rx_idx;
volatile uint8_t	rs232_rx_flag;
volatile uint8_t	rs232_tx_buffer[50];
volatile uint8_t 	rs232_tx_idx;
volatile uint8_t	rs232_tx_flag;
volatile uint8_t 	rs232_tx_len;

enum BAUD {B9600 = 103, B19200 = 51, B38400 = 25, B57600 = 16, B115200 = 8}; // U2X = 0
enum DATA {D7 = 2, D8 = 3};
enum PARITE {PN = 0, PP = 2, PI = 3}; // sans parité, parité paire, parité impaire
enum STOP {STP1 = 0, STP2 = 1};		// 1 stop, 2 stop

/* UCSRA's bits
 * Constant	bit
 * RXC		7	// Receive Complete flag
 * TXC		6	// Transmit Complete flag
 * UDRE		5	// Data Register Empty flag
 * FE		4	// Frame Error flag
 * DOR		3	// Data OverRun, always set this bit to zero when writing to UCSRA.
 * UPE		2	// Parity Error, always set this bit to zero when writing to UCSRA.
 * U2X		1	// Double the USART Transmission Speed
 * MPCM		0	// Multi-Processor Communication Mode
 *
 * UCSRB's bits
 * RXCIE	7	// RX Complete Interrupt Enable
 * TXCIE	6	// TX Complete Interrupt Enable
 * UDRIE	5	// USART Data Register Empty Interrupt Enable
 * RXEN		4	// Receiver Enable
 * TXEN		3	// Transmitter Enable
 * UCSZ2	2	// Character size bit 2 (bits 1 et 0 dans UCSRC)
 * RXB8		1	// Receive Data Bit 8
 * TXB8		0	// Transmit Data Bit 8
 *
 * UCSRC's bits
 * RES		7	// Reserved, must be set to zero
 * UMSEL	6	// USART Mode Select, 0 = Asynchronous Operation
 * UPM1		5	// Parity Mode bit 1
 * UPM0		4	// Parity Mode bit 0
 * 		UPM Bits Settings
 * 		UPM1	UPM0	Parity Mode
 * 		0		0		Disabled
 * 		0		1		(Reserved)
 * 		1		0		Enabled, Even Parity
 * 		1		1		Enabled, Odd Parity
 *
 * USBS		3	// Stop Bit Select, 0: 1 stop bit, 1: 2 stop bits
 * UCSZ1	2	// Character Size bit 1 (bit 2 dans UCSRB)
 * UCSZ0	1	// Character Size bit 0
 * 		UCSZ Bits Settings
 * 		UCSZ2	UCSZ1	UCSZ0	Character Size
 * 		0		1		0		7 bits
 * 		0		1		1		8 bits
 *
 * UCPOL	0	// Clock Polarity, for synchronous mode only, set to 0 when asynchronous
 */

void rs232_init(enum BAUD, enum DATA, enum PARITE, enum STOP);	// port, vitesse, data, parité, stop

#define RS232_TX\
	rs232_tx_flag = 1;\
	rs232_tx_idx = 0;\
	UDR0 = rs232_tx_buffer[rs232_tx_idx++] /* Put data into buffer */

#define RS232_FWD\
	UDR0 = rs232_tx_buffer[rs232_tx_idx++];\
	if(rs232_tx_idx == rs232_tx_len)\
	{\
		rs232_tx_len = 0;\
		rs232_tx_idx = 0;\
		rs232_tx_flag = 0;\
	}

#endif /* RS232_H_ */
